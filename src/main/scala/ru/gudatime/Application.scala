package ru.gudatime

import org.apache.poi.openxml4j.opc.{PackageAccess, OPCPackage}
import org.apache.poi.xssf.usermodel.XSSFWorkbook

import scala.collection.JavaConversions.asScalaBuffer
import scala.collection.JavaConversions.iterableAsScalaIterable
import scala.collection.JavaConversions.bufferAsJavaList
import org.apache.poi.ss.usermodel.Row
import POIUtils._
import scala.collection.{mutable, Map}
import scala.collection.mutable.ArrayBuffer
import scala.collection.immutable.{ListMap, IndexedSeq}
import java.io.{FileOutputStream, File}
import com.thoughtworks.xstream.XStream
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver
import scala.concurrent._
import scala.concurrent.duration.Duration
import scala.concurrent.ExecutionContext.Implicits.global

object Application {

  val daysOfWeek = Set("Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье")


  def main(args: Array[String]) {

    def arg(i: Int) = if(args.isDefinedAt(i)) Some(args(i)) else None

    val file = new File(arg(0).getOrElse("Статистика по входящим звонкам.xlsx"))
    println("file:" + file.getAbsolutePath)
    val predictedDailyLoad = getDailyLoad(file).slice(1, 2)

    val func = new ErlangC(
      callTime = 122,
      warmUpTime = 30,
      acceptableDelay = 20,
      serviceLevelRequirement = 95
    )

    predictedDailyLoad.foreach(println)
    println()

    val erc = predictedDailyLoad.mapValues(_.map(l => func(l)))

    erc.foreach(println)


    val optimizedDayEntries = genOptimized(erc.mapValues(plan => {
      val tabler = new TimeTabler(plan)
      tabler.standardTurnsRatioDeviation = StandardTurns.check(Seq(0.20, 0.00, 0.20))
      tabler
    }

    ))
    val writer = new TimeTableWriter(7,predictedDailyLoad.values, optimizedDayEntries)
    writer.genTimeTable()
    writer.genStatistics(func)
    writer.workbook.write(new FileOutputStream("out.xlsx"))

  }

  def genOptimized(daylyload: Map[String, TimeTabler]): Iterable[DayEntry] = {
    val futures = for ((day, srcTabler) <- daylyload) yield future {
      println("processing:" + day)
      val optimized = OptaOptimizer.optimizeTimeTabler(srcTabler)
      println(day + " " + optimized.getScore + " :" +
        optimized.turns.map(t => s"Turn(${t.startHour}, ${t.endHour}, hourBreak= ${t.hourBreak})").mkString(", "))

      optimized.turns = optimized.turns.filter(_.turnLength > 0)
      DayEntry(day, srcTabler.plan.toIndexedSeq, optimized)
    }

    val result = Await.result(Future.sequence(futures), Duration.Inf)
    println("sumresult = "+result.map(_.table.score.getSoftScore).sum)
    result
  }


  def getDailyLoad(file: File): Map[String, IndexedSeq[Int]] = {
    val opcPackage = OPCPackage.open(file, PackageAccess.READ);
    try {
      val workbook = new XSSFWorkbook(opcPackage)
      val sheet = workbook.getSheetAt(0)

      val rows = sheet.filter(_.getCell(0).nonEmpty)

      val resmap = groupByDays(rows)

      //println(resmap.mapValues(_.map(_.getCell(0).getStringCellValue)))

      val perDayLoadHistory = resmap.mapValues(dayrows =>
        dayrows
          .filter(_.getCell(0).getStringCellValue.contains("недел"))
          .filter(_.drop(1).take(15).exists(_.nonEmpty))
          .map(row =>
          row.drop(1).map(
            cell => cell.getNumericCellValue.toInt
          ).toIndexedSeq
        ))


      //      val daylyLoad =  if (minHistorySize < 4) predictedByAverage(perDayLoadHistory)
      //      else
      //        predictedByRegression(perDayLoadHistory)
      val daylyLoad = ListMap.empty ++ (for ((day, data) <- perDayLoadHistory.toIterable) yield (day, {
        val filtred = data.filter(_.forall(_ > 0))
        val avarege = predictDayByAvarege(filtred)
        if (filtred.size < 4) {
          avarege
        } else
          predictDayByRegression(filtred).zip(avarege).map(math.max _ tupled)
      }))
      println("daylyLoad="+daylyLoad)
      daylyLoad
    }
    finally {
      opcPackage.close()
    }
  }


  private[this] def predictDayByAvarege(data: ArrayBuffer[IndexedSeq[Int]]): IndexedSeq[Int] = {

    val right = data.takeRight(3)
    val sum = right.reduce((r1, r2) => r1.zip(r2).map(z => z._1 + z._2))
    val lastThreeAvr = sum.map(i => (i.toFloat / right.length).round)
    lastThreeAvr.dropRight(1)

  }


  private[this] def predictDayByRegression(data: ArrayBuffer[IndexedSeq[Int]]): IndexedSeq[Int] = {


    val intervalIndexes = data.head.indices.dropRight(1)
    val lastWeeks = data.takeRight(5)
    val pred = intervalIndexes.map(i => {
      val hours = lastWeeks.map(_(i))
      val result = Extrapolator.predictNext(hours)
      if(result < 0)
         System.err.println(" negative prediction " + " " + hours)
      result
    })
    pred

  }

  def groupByDays(rows: Iterable[Row]) = {
    var headingValue: String = null

    val resmap = new mutable.LinkedHashMap[String, ArrayBuffer[Row]]() {
      override def default(key: String) = {
        val r = new ArrayBuffer[Row]()
        this(key) = r
        r
      }
    }

    for (row <- rows) {
      if (daysOfWeek(row.getCell(0).getStringCellValue))
        headingValue = row.getCell(0).getStringCellValue
      else
        resmap(headingValue) += row
    }

    resmap
  }
}
