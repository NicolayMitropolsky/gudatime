package ru.gudatime

import org.apache.commons.math3.distribution.PoissonDistribution

import scala.util.control.Breaks
import scala.collection.mutable.ArrayBuffer

object ErlangC {


  def main(args: Array[String]) {

    //Среднее время постобработки
    val callsPerHour = 5 //Средняя допустимая задержка


    val func = new ErlangC(
      callTime = 122,
      warmUpTime = 30,
      acceptableDelay = 20,
      serviceLevelRequirement = 95
    )

    val cEntries = func.genErlangCSeq(callsPerHour)
    cEntries.foreach(println)
    println()
    println(func(callsPerHour))

    //    val WT = callTime + warmUpTime
    //
    //    val result = genErlangCSeq(WT, callsPerHour, acceptableDelay)
    //
    //
    //    result.foreach(println)

    println(ErlangC2.getEntry(360, 1800, 240, 49, 15))

  }

  /**
   *
   * @param avrProcessingTime average processing time in sec
   * @param callsPerHour calls per Hour
   * @param acceptableDelay average acceptable delay in sec
   * @return
   */
  def genErlangCSeq(avrProcessingTime: Int, callsPerHour: Int, acceptableDelay: Int): Seq[ErlangCEntry] = {
    val Ergs = (avrProcessingTime * callsPerHour) / 3600.toFloat
    var N = Math.round(Ergs)
    if (N == 0)
      N = 1
    var T = 1f
    var T1 = 1f
    var NS = 1f
    val ST = 1

    val result = new ArrayBuffer[ErlangCEntry]

    Breaks.breakable {
      do {
        if (N > Ergs) {
          val T2 = T * (Ergs / N) * (N / (N - Ergs))
          val P = T2 / (T1 + T2)
          //val DLY = (1 / (N - Ergs)) * avrProcessingTime
          val ASA = (P * (1 / (N - Ergs))) * avrProcessingTime
          val Q2 = Ergs * (1 / (N - Ergs))
          val Q1 = P * Q2
          val F = acceptableDelay.toFloat / avrProcessingTime
          val SL = 1 - (P / Math.exp(F / (1 / (N - Ergs)))).toFloat

          val totalNoAnwer = (P / Math.exp((N - Ergs) * acceptableDelay * 2 / avrProcessingTime)).toFloat

          require(totalNoAnwer + SL <= 1)

          //val TKLD = ((TT + ASA) * callsPerHour) / 3600
          val OCC = Ergs / N

          result += ErlangCEntry(
            operatorsNumber = N,
            noQueueProbability = Math.round(100 - P * 100),
            noAnswerProbability = totalNoAnwer *100,// Math.round(100 - P*Pnoanswer.toFloat * 100),
            avrWait = Math.round(ASA),
            awrQueue = Math.round(Q1),
            srvLvl = Math.round(SL * 100),
            operatorOccupation = Math.round(OCC * 100)
          )

          if (P < 0.00001)
            Breaks.break()
        }

        //println()

        T *= Ergs / N
        T1 += T
        N += ST
        NS = NS + 1
      } while (N < 200000)


    }

    result
  }
}

/**
 *
 * @param operatorsNumber number of operators
 * @param noQueueProbability probability of processing without queuing
 * @param avrWait average time to wait before answer (in sec)
 * @param awrQueue average queue length
 * @param srvLvl percentage of calls waiting in queue less than average acceptable delay
 * @param operatorOccupation percentage of time operator spent working
 */
case class ErlangCEntry(
                         operatorsNumber: Int,
                         noQueueProbability: Int,
                         noAnswerProbability: Float,
                         avrWait: Int,
                         awrQueue: Int,
                         srvLvl: Int,
                         operatorOccupation: Int
                         )

class ErlangC(
               val callTime: Int, //Среднее время разговора
               val warmUpTime: Int, //Среднее время постобработки
               val acceptableDelay: Int, //Средняя допустимая задержка
               val serviceLevelRequirement: Int
               ) extends (Int => Int) {
  def genErlangCSeq(callsPerHour: Int) = ErlangC.genErlangCSeq(callTime + warmUpTime, callsPerHour, acceptableDelay)

  def apply(callsPerHour: Int): Int = {
    val erlang = genErlangCSeq(callsPerHour).find(_.srvLvl >= serviceLevelRequirement)
      .getOrElse(throw new IllegalArgumentException(s"callsPerHour=$callsPerHour has not acceptable serviceLevelRequirement"))
    println("callsPerHour=" + callsPerHour + " erlang=" + erlang)
    erlang.operatorsNumber
  }
}

object ErlangC2 {

  def genErlangCSeq(avrProcessingTime: Int, callsPerHour: Int, acceptableDelay: Int): Seq[ErlangCEntry] = {
    val i = Iterator.from(1)
      .map(numberOfAgents => getEntry(callsPerHour, 3600, avrProcessingTime, numberOfAgents, acceptableDelay)).filter(_.srvLvl > 0)

    def f: Stream[ErlangCEntry] = {
      val v = i.next()
      if (v.srvLvl == 100)
        Stream(v)
      else
        Stream.cons(v, f)
    }

    f
  }

  def getEntry(callsPerInterval: Int, interval: Int, callDuration: Int, numberOfAgents: Int, targetAnswer: Int) = {
    val trafficIntensity = callsPerInterval.toFloat * callDuration / interval
    val agentOccupation = trafficIntensity / numberOfAgents
    val p = new PoissonDistribution(trafficIntensity)
    val erlangC = p.probability(numberOfAgents) / (p.probability(numberOfAgents) + (1 - agentOccupation) * p.cumulativeProbability(numberOfAgents - 1))
    val srvlvl = 1 - erlangC * Math.exp(-(numberOfAgents - trafficIntensity) * targetAnswer / callDuration)
    val agenOccupancyPersent = (agentOccupation * 100).round.toInt
    val intermediateAnswer = Math.round((1 - erlangC) * 100).toInt
    val srvlevelPersent = (srvlvl * 100).round.toInt
    val awarageanswerSpeed = (erlangC * callDuration / (numberOfAgents * (1 - agentOccupation))).round.toInt

    val noanswerProb = erlangC * Math.exp(-(numberOfAgents - trafficIntensity) * targetAnswer * 2 / callDuration)
    ErlangCEntry(
      operatorsNumber = numberOfAgents,
      noQueueProbability = intermediateAnswer,
      noAnswerProbability = Math.round(noanswerProb * 100),
      avrWait = awarageanswerSpeed,
      awrQueue = -1, // Math.round(Q1),
      srvLvl = srvlevelPersent,
      operatorOccupation = agenOccupancyPersent
    )
  }

}

