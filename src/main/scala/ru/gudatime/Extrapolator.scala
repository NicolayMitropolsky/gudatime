package ru.gudatime

import org.apache.commons.math3.stat.regression.SimpleRegression

/**
 * Created with IntelliJ IDEA.
 * User: nickl
 * Date: 09.11.13
 * Time: 13:42
 * To change this template use File | Settings | File Templates.
 */
object Extrapolator {


  def predictNext(data: Seq[Int]):Int = {
    val regression = new SimpleRegression()

    for ((v,i) <- data.zipWithIndex ) {
      regression.addData(i,v)
    }

    regression.predict(data.size).round.toInt
  }

  def main(args: Array[String]) {

     println(predictNext(Seq(1,2,3,4,5)))
     println(predictNext(Seq(2,4,6,8,10)))
     println(predictNext(Seq(10,9,8,7,6)))
     println(predictNext(Seq(10,10,10,10,10)))
     println(predictNext(Seq(0,0,0,0,0)))

     println(predictNext(Seq(4,3,4,3,4,3,4,3)))

  }

}
