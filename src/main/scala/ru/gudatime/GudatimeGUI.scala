package ru.gudatime

import javax.swing._
import java.awt._
import java.util.concurrent.atomic.AtomicInteger
import java.awt.event._
import utils.SwingUtils._
import java.io._
import scala.concurrent.{Future, ExecutionContext, future}
import scala.Console
import javax.swing.text.{DefaultEditorKit, JTextComponent}
import javax.swing.event.ChangeEvent
import ru.gudatime.utils.{SwingUtils, IntFilter}


/**
  * Created with IntelliJ IDEA.
  * User: nickl
  * Date: 13.07.13
  * Time: 17:14
  * To change this template use File | Settings | File Templates.
  */
object GudatimeGUI extends GudatimeGUIView {

  import ExecutionContext.Implicits.global

  import Application._


  lazy val props = new FiledProperties(
    new File(new File(this.getClass.getProtectionDomain.getCodeSource.getLocation.toURI).getParentFile, "conf.properties")
  )

  def init() {
    createGUI()

    this.exeBtn.addActionListener(exec _)

    loadAndSaveProperties()

    frame.setVisible(true)
  }


  private[this] def loadAndSaveProperties() {
    for (x <- props.get("frame.x");
         y <- props.get("frame.y");
         width <- props.get("frame.width");
         height <- props.get("frame.height")
    ) {
      frame.setBounds(x.toInt, y.toInt, width.toInt, height.toInt)
    }

    //    srcStatField.setText(props.getOrElse("srcStat", ""))
    //    outFileField.setText(props.getOrElse("outFile", ""))


    val fields = this.getClass.getDeclaredFields.toSeq.filter(f => f.getName.endsWith("Field"))

    println(fields.map(_.getName))

    for (field <- fields) {
      props.get(field.getName.stripSuffix("Field")).foreach(fv => {
        println(fv)
        field.setAccessible(true)
        field.get(this) match {
          case f: JTextField => f.setText(fv)
          case f: JSlider => f.setValue(fv.toInt)
        }
      })
    }


    frame.addWindowListener(new java.awt.event.WindowAdapter() {
      override def windowClosing(windowEvent: java.awt.event.WindowEvent) {

        val exitTask = future {
          val bounds = frame.getBounds
          props("frame.x") = bounds.getX.toInt.toString
          props("frame.y") = bounds.getY.toInt.toString
          props("frame.width") = bounds.getWidth.toInt.toString
          props("frame.height") = bounds.getHeight.toInt.toString

          for (field <- fields) {
            field.setAccessible(true)
            val text = field.get(GudatimeGUI.this) match {
              case f: JTextField => f.getText
              case f: JSlider => f.getValue.toString
            }
            props(field.getName.stripSuffix("Field")) = text
          }
          props.save()
        }

        exitTask.onFailure {
          case e => e.printStackTrace()
        }

        exitTask.onComplete {
          case _ => println("finished"); System.exit(0)
        }
      }
    })
  }

  def enableExecBtn(enable: Boolean) {
    exeBtn.setEnabled(enable)
    frame.getContentPane.setEnabled(enable)
  }

  def exec() {
    val srcFile = new File(srcStatField.getText)

    if (!srcFile.exists()) {
      JOptionPane.showMessageDialog(this.frame, "Файл не существует " + srcStatField.getText, "Ошибка", JOptionPane.ERROR_MESSAGE)
      return
    }


    val f = executeTask {
      val avrDailyLoad = getDailyLoad(srcFile)

      val func = new ErlangC(
        callTime = callTimeField.intValue,
        warmUpTime = 0,
        acceptableDelay = acceptableDelayField.intValue,
        serviceLevelRequirement = serviceLevelRequirementField.intValue
      )

      avrDailyLoad.foreach(println)
      println()

      val erc = avrDailyLoad.mapValues(_.map(l => func(l)))

      erc.foreach(println)
      println

      val toOptimize = erc.mapValues(load => {
        val tabler = new TimeTabler(load)
        tabler.excessPenalty = excessPenaltyField.intValue
        tabler.busyWorkerPenalty = busyWorkerPenaltyField.intValue
        tabler.lackPenalty = lackPenaltyField.intValue
        tabler.standardTurnsRatioDeviation = StandardTurns.check(Seq(
          standardTurnsRatio1Field.getValue,
          standardTurnsRatio2Field.getValue,
          standardTurnsRatio3Field.getValue
        ).map(_ / 100.0))
        tabler
      })


      val targetName = outFileField.getText.stripSuffix(".xlsx")
      val file = (Iterator(targetName) ++ Iterator.from(1).map(targetName + _)).map(n => new File(n + ".xlsx")).filter(!_.exists()).next()

      val outputStream = new FileOutputStream(file)
      val optimized = genOptimized(toOptimize)
      val table = new TimeTableWriter(this.startHourField.intValue, avrDailyLoad.values, optimized)
      table.genTimeTable()
      table.genStatistics(func)
      table.workbook.write(outputStream)
      optimized
    }

    f.onSuccess {
      case optimized =>
        val imposibleDays = optimized.filter(_.table.score.getHardScore < 0).map(_.day)
        invokeInSwing {
        if (imposibleDays.isEmpty)
          JOptionPane.showMessageDialog(this.frame, "Выполнено. Штраф: " + optimized.map(_.table.score.getSoftScore).sum, "Выполнено", JOptionPane.INFORMATION_MESSAGE)
        else
          JOptionPane.showMessageDialog(this.frame, s"Не найдено решение в ${imposibleDays.mkString(", ")}", "Не выполнено", JOptionPane.WARNING_MESSAGE)
      }
    }


  }

  def executeTask[T](fu: => T): Future[T] = {
    enableExecBtn(false)
    frame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR))
    val f = future {

      try {
        fu
      }
      catch {
        case e: Throwable => {
          invokeInSwing {
            JOptionPane.showMessageDialog(this.frame, e.getMessage, "Ошибка", JOptionPane.ERROR_MESSAGE)
          }
          Console.out.println(e.getMessage)
          e.printStackTrace()
          throw e
        }
      }

    }
    f onComplete {
      case _ =>
        enableExecBtn(true)
        frame.setCursor(Cursor.getDefaultCursor)
    }
    f
  }

  def main(args: Array[String]) {

    import util.control._

    Breaks.breakable {
      val r = Stream("com.sun.java.swing.plaf.gtk.GTKLookAndFeel", UIManager.getSystemLookAndFeelClassName).foreach(arg =>
        try {
          UIManager.setLookAndFeel(arg)
          Breaks.break()
        }
        catch {
          case e: UnsupportedLookAndFeelException => false
          case e: ClassNotFoundException => false
          case e: InstantiationException => false
          case e: IllegalAccessException => false
        }
      )
    }


    javax.swing.SwingUtilities.invokeLater(new Runnable {
      def run {
        init()
      }
    })
  }


}

trait GudatimeGUIView {

  lazy val frame: JFrame = new JFrame("Генератор смен")

  lazy val jTextArea: JTextArea = new JTextArea

  lazy val srcStatField: JTextField = new JTextField("")

  lazy val outFileField: JTextField = new JTextField("")

  lazy val exeBtn: JButton = new JButton("Сгенерировать")

  lazy val callTimeField = new JTextField("122") with IntFilter
  lazy val acceptableDelayField = new JTextField("20") with IntFilter
  lazy val serviceLevelRequirementField = new JTextField("95") with IntFilter
  lazy val standardTurnsRatio1Field = new JSlider(0, 100)
  lazy val standardTurnsRatio2Field = new JSlider(0, 100)
  lazy val standardTurnsRatio3Field = new JSlider(0, 100)
  lazy val startHourField = new JTextField("7") with IntFilter
  lazy val excessPenaltyField = new JTextField("5000") with IntFilter

  lazy val lackPenaltyField = new JTextField("225000") with IntFilter

  lazy val busyWorkerPenaltyField = new JTextField("20") with IntFilter

  def addComponentsToPane(pane: Container) {

    pane.setLayout(new GridBagLayout)
    val line = new AtomicInteger(0)
    val coll = new AtomicInteger(0)
    addFileComponent(srcStatField, pane, line.getAndIncrement, "Файл со статистикой", None)
    addFileComponent(outFileField, pane, line.getAndIncrement, "Вывод", None)
    val afterHighGridY = line.get

    def left = new GridBagConstraints {
      fill = GridBagConstraints.HORIZONTAL
      gridx = coll.incrementAndGet()
      gridy = line.get - 1
    }


    def newLine(x: Int = 0): GridBagConstraints = {
      coll.set(x)
      new GridBagConstraints {

        fill = GridBagConstraints.HORIZONTAL
        gridx = x
        gridy = line.getAndIncrement
      }
    }


    pane.add(new JLabel("Настройки:"), newLine())
    pane.add(new JLabel("Час начала работы"), newLine())
    pane.add(startHourField, left)
    pane.add(new JLabel("Параметры ErlangC:"), newLine())
    pane.add(new JLabel("Время разговора"), newLine())
    pane.add(callTimeField, left)
    pane.add(new JLabel("Допустимая задержка"), newLine())
    pane.add(acceptableDelayField, left)
    pane.add(new JLabel("Уровень обслуживания"), newLine())
    pane.add(serviceLevelRequirementField, left)
    pane.add(new JLabel("Процент стандартных смен (5/2)"), newLine())
    pane.add(standardTurnsRatio1Field, left)
    pane.add(new JLabel("Процент стандартных смен (2/2)"), newLine())
    pane.add(standardTurnsRatio2Field, left)
    pane.add(new JLabel("Процент стандартных смен (\"2 смена\")"), newLine())
    pane.add(standardTurnsRatio3Field, left)
    pane.add(new JLabel("Штрафы:"), newLine())
    pane.add(new JLabel("Избыточность"), newLine())
    pane.add(excessPenaltyField, left)
    pane.add(new JLabel("Недостаточность"), newLine())
    pane.add(lackPenaltyField, left)
    pane.add(new JLabel("Дополнительная ставка"), newLine())
    pane.add(busyWorkerPenaltyField, left)

    pane.add(Box.createGlue(), new GridBagConstraints {
      fill = GridBagConstraints.BOTH
      gridx = 0
      gridy = line.getAndIncrement
      weightx = 1
      weighty = 1
      gridheight = line.get() - afterHighGridY + 1
      gridwidth = 2
    })

    line.set(3)
    pane.add(exeBtn, newLine(2))

  }

  private def parentFileOpt(str: String) = Option(new File(str)).filter(_.exists()).flatMap(f => Option(f.getParentFile))

  private def addFileComponent(plmField: JTextField, pane: Container, i: Int, name: String, pathProvider: => Option[String]): JTextField = {
    val c: GridBagConstraints = new GridBagConstraints
    c.fill = GridBagConstraints.HORIZONTAL
    c.gridx = 0
    c.gridy = i
    pane.add(new JLabel(name), c)
    c.gridx = 1
    c.gridy = i
    c.weightx = 1
    c.ipadx = 300
    plmField.setSize(50, 50)
    pane.add(plmField, c)
    c.gridx = 2
    c.gridy = i
    c.weightx = 0
    c.ipadx = 0
    val fcbtn = new JButton("Выбрать")
    val fc = new JFileChooser()
    fcbtn.addActionListener((e: ActionEvent) => {
      parentFileOpt(plmField.getText).orElse(pathProvider.flatMap(parentFileOpt)).foreach(f =>
        fc.setCurrentDirectory(f)
      )
      val returnVal = fc.showOpenDialog(frame)
      if (returnVal == JFileChooser.APPROVE_OPTION) {
        plmField.setText(fc.getSelectedFile.getAbsolutePath)
      }
    })
    pane.add(fcbtn, c)
    return plmField
  }

  protected def createGUI() {
    frame.setLocationRelativeTo(null)
    frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE)
    val contentPane: JPanel = frame.getContentPane.asInstanceOf[JPanel]
    contentPane.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3))
    addComponentsToPane(contentPane)
    frame.pack
  }

}

class Properties extends collection.mutable.Map[String, String] {

  import java.util.{Properties => JProp}
  import collection.JavaConversions.asScalaIterator

  val jprop = new JProp()

  def load(in: InputStream): this.type = {
    jprop.load(in);
    this
  }

  def load(in: Reader): this.type = {
    jprop.load(in);
    this
  }

  def load(in: File): this.type = {
    val ins = new BufferedInputStream(new FileInputStream(in))
    val r = load(new InputStreamReader(ins, "UTF-8"))
    ins.close()
    r
  }

  def load(in: String): this.type = load(new File(in))

  def store(out: OutputStream): this.type = {
    jprop.store(out, "");
    this
  }

  def store(out: Writer): this.type = {
    jprop.store(out, "");
    this
  }

  def store(in: File): this.type = {
    val in1 = new PrintWriter(in, "UTF-8")
    store(in1)
    in1.close()
    this
  }

  def list(in: String): this.type = store(new File(in))

  def +=(kv: (String, String)) = {
    jprop.put(kv._1, kv._2);
    this
  }

  def -=(key: String) = {
    jprop.remove(key);
    this
  }

  def get(key: String) = {
    Option(jprop.get(key).asInstanceOf[String])
  }

  def iterator = jprop.entrySet().iterator().map(kv => (kv.getKey.asInstanceOf[String], kv.getValue.asInstanceOf[String]))
}

class FiledProperties(file: File) extends Properties {

  if (file.exists())
    load(file)

  def save() = store(file)

}

class TextAreaWriter(jTextArea: JTextArea) extends Writer {
  def flush() {}

  def write(cbuf: Array[Char], off: Int, len: Int) {
    //jTextArea.getDocument.insertString(0, new String(cbuf.drop(off).take(len)), null)
    val s = new String(cbuf.drop(off).take(len))
    invokeInSwing(jTextArea.append(s))
  }

  def close() {}
}