package ru.gudatime



import java.util.Collections
import java.util

import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore

import scala.beans.BeanProperty
import org.optaplanner.core.api.domain.solution.{PlanningEntityCollectionProperty, PlanningSolution, Solution}
import org.optaplanner.core.api.domain.entity.PlanningEntity
import org.optaplanner.core.api.domain.valuerange.{CountableValueRange, ValueRangeFactory, ValueRangeProvider}

import scala.collection.JavaConversions.{asScalaBuffer, seqAsJavaList}
import org.optaplanner.core.api.domain.variable.PlanningVariable

import scala.annotation.meta.beanGetter
import org.optaplanner.core.api.score.buildin.hardsoftdouble.HardSoftDoubleScore
import org.optaplanner.core.api.solver.SolverFactory
import org.optaplanner.core.impl.domain.valuerange.buildin.collection.ListValueRange
import org.optaplanner.core.impl.domain.valuerange.buildin.composite.CompositeCountableValueRange
import org.optaplanner.core.impl.heuristic.move.{AbstractMove, Move}
import org.optaplanner.core.impl.heuristic.selector.common.decorator.SelectionFilter
import org.optaplanner.core.impl.heuristic.selector.move.factory.MoveListFactory
import org.optaplanner.core.impl.heuristic.selector.move.generic.ChangeMove
import org.optaplanner.core.impl.score.director.ScoreDirector
import org.optaplanner.core.impl.score.director.easy.EasyScoreCalculator
import ru.gudatime.utils.OptaPlannerUtils.CountableValueRangesOpts

import scala.collection.JavaConverters.asScalaIteratorConverter


/**
 * Created with IntelliJ IDEA.
 * User: nickl
 * Date: 14.08.13
 * Time: 18:12
 * To change this template use File | Settings | File Templates.
 */
object OptaOptimizer {
  def main(args: Array[String]) {

    val load = Vector(4, 11, 13, 14, 14, 11, 11, 11, 12, 11, 10, 6, 5, 3, 3)

    val solution: TimeTabler = optimizeTimeTabler(new TimeTabler(load))
    println("solution=" + solution)

  }


  def optimizeTimeTabler(tabler:TimeTabler): TimeTabler = {
    val solverFactory = SolverFactory.createFromXmlResource[TimeTabler]("ru/gudatime/cong.xml")
    //    val config = new TerminationConfig
    //    config.setMaximumSecondsSpend(time)
    //    solverFactory.getSolverConfig.setTerminationConfig(config)

    val solver = solverFactory.buildSolver()


    solver.solve(tabler)
    val solution = solver.getBestSolution
    solution
  }

}

@PlanningSolution
class TimeTabler(val plan: Seq[Int]) extends Solution[HardSoftDoubleScore] {

  private def this() = this(null)

  var worklength = Option(plan).map(_.length).getOrElse(-1)

  var excessPenalty = 5000

  var lackPenalty = 225000

  var busyWorkerPenalty = 20

  var standardTurnsRatioDeviation: (Seq[Turn]) => Double = (any) => 0.0

  @BeanProperty
  var score: HardSoftDoubleScore = null

  @(PlanningEntityCollectionProperty@beanGetter)
  @BeanProperty
  var turns: util.List[Turn] = new util.ArrayList[Turn]() // if (plan != null) Seq.fill(plan.max * 2)(new Turn) else null

  def turnsOfTheDay = getTurns.filter(!_.free).sortBy(t => (-t.turnLength,t.getStartHour))

  @(ValueRangeProvider@beanGetter)(id = "startHour")
  @BeanProperty
  val startHours = ValueRangeFactory.createIntValueRange(0, if (worklength != -1) worklength - 4 else 0)

  @(ValueRangeProvider@beanGetter)(id = "endHour")
  @BeanProperty
  val endHours = ValueRangeFactory.createIntValueRange(4, if (worklength != -1) worklength + 1 else 4)

  @(ValueRangeProvider@beanGetter)(id = "hourBreak")
  @BeanProperty
  val hourBreaks: CountableValueRange[Int] = new ListValueRange[Int](-1 +: Seq.range(6, 12))

  def getProblemFacts: util.Collection[_] = Collections.emptyList()

  override def toString = "TimeTabler(score=" + score + "," + turns.filter(!_.free).mkString(",") + ")"


}

@PlanningEntity
class Turn {

  @(PlanningVariable@beanGetter)(valueRangeProviderRefs = Array("startHour"))
  @BeanProperty
  var startHour: Int = 4

  @(PlanningVariable@beanGetter)(valueRangeProviderRefs = Array("hourBreak"))
  @BeanProperty
  var hourBreak: Int = -1

  def turnLength: Int = endHour - startHour

  @(PlanningVariable@beanGetter)(valueRangeProviderRefs = Array("endHour"))
  @BeanProperty
  var endHour: Int = 4

  def worksAt(h: Int) = h != hourBreak && h >= startHour && h < endHour

  def hasBreak = hourBreak >= startHour && hourBreak < endHour

  def free = !busy

  def busy = turnLength > 0

  def workingHours = Range(startHour, endHour)

  ////  @(ValueRangeProvider@beanGetter)(id = "endHour")
  ////  @BeanProperty
  //  @ValueRangeProvider(id = "endHour")
  //  def hoursEnds: util.List[Int] = startHour to (startHour + 12)

  override def toString = s"Turn($startHour,$turnLength,break=$hourBreak)"

}

object Turn{
  def apply(startHour: Int, endHour: Int, hourBreak: Int): Turn = {
    val turn = new Turn()
    turn.startHour = startHour
    turn.endHour = endHour
    turn.hourBreak = hourBreak
    turn
  }
}

class TimeTableScoreCalc extends EasyScoreCalculator[TimeTabler] {
  def calculateScore(solution: TimeTabler): HardSoftDoubleScore = {

    val plan = solution.plan
    val turns = solution.getTurns
    val busyTurns = turns.filter(_.turnLength > 0)

    val work = plan.indices.map(i => turns.count(_.worksAt(i)))
    val plansZip = work.zip(plan)
    val missPenalty = Math.sqrt(plansZip.map(v => (v._2, (v._1 - v._2))).map({
      case (v, d) if (d.toDouble / v) >= 0 => val r = d.toDouble / v
        r * r * solution.excessPenalty
      case (v, d) if (d.toDouble / v) < 0 => val r = d.toDouble / v
        r * r * solution.lackPenalty
    }).sum)
    //println(sum)

    val overwork = busyTurns.count(t => t.turnLength < 4 || t.turnLength > 12)
    val invalidTime = busyTurns.count(t => t.startHour < 0 || t.endHour > solution.worklength)

    val lessThanHalf = plansZip.map(v => Math.max(0, v._2 - v._1 * 2)).sum

    val invalidBreaks = busyTurns.count(invalidBreak)

    val planWorkers = plan.max
    val busyWorkers = busyTurns.size

    val plannedHours = plan.sum
    val totalHours =  busyTurns.map(_.turnLength).sum

    val lessThanSix = busyTurns.map(_.turnLength).filter(_ < 9).size

    val earlyShort = busyTurns.filter(t => t.startHour < (solution.worklength / 2 + 2) && t.turnLength < 9).size

    val standardTurnsDeviation = solution.standardTurnsRatioDeviation(busyTurns)

    HardSoftDoubleScore.valueOf(
      -overwork - invalidTime - lessThanHalf - invalidBreaks - Math.max(0, standardTurnsDeviation),
      -missPenalty * 1 - totalHours * 0.5 - busyWorkers * solution.busyWorkerPenalty - lessThanSix * 10 - earlyShort * 20)
  }

  def invalidBreak(t: Turn): Boolean = {
      if (t.turnLength >= 9)
        !(t.hourBreak > t.startHour + 3 && t.hourBreak < t.endHour - 3)
      else
        t.hasBreak
  }
}


//class StartTimeChangeFilter extends SelectionFilter[ChangeMove]{
//  override def accept(scoreDirector: ScoreDirector, move: ChangeMove): Boolean = {
//    val turn = move.getEntity.asInstanceOf[Turn]
//    val v = move.getToPlanningValue.asInstanceOf[Int]
//    turn.busy || v < turn.endHour
//  }
//}
//
//class EndTimeChangeFilter extends SelectionFilter[ChangeMove]{
//  override def accept(scoreDirector: ScoreDirector, move: ChangeMove): Boolean = {
//    val turn = move.getEntity.asInstanceOf[Turn]
//    val v = move.getToPlanningValue.asInstanceOf[Int]
//    turn.busy || v > turn.startHour
//  }
//}
//
//class HourBreakChangeFilter extends SelectionFilter[ChangeMove]{
//  override def accept(scoreDirector: ScoreDirector, move: ChangeMove): Boolean = {
//    val turn = move.getEntity.asInstanceOf[Turn]
//    val v = move.getToPlanningValue
//    turn.busy
//  }
//}


class StartEndMoveListFactory extends MoveListFactory[TimeTabler] {

  val range = -8 to 8
  val params = Seq("startHour", "endHour")

  override def createMoveList(solution: TimeTabler): util.List[_ <: Move] = {
    val turns = solution.getTurns
    (for (turn <- turns;
          param <- params;
          shift <- range)
      yield new StartEndMoveChangeMove(turn, param, shift)).toVector
  }
}

class StartEndMoveChangeMove(turn: Turn, param: String, value: Int) extends AbstractMove {

  //println(s"StartEndMoveChangeMove($param, $value, $turn)")

  override def doMoveOnGenuineVariables(scoreDirector: ScoreDirector): Unit = {
    scoreDirector.beforeVariableChanged(turn, param)
    param match {
      case "startHour" => turn.startHour += value
      case "endHour" => turn.endHour += value
    }
    scoreDirector.afterVariableChanged(turn, param)
  }

  override def createUndoMove(scoreDirector: ScoreDirector): Move
  = new StartEndMoveChangeMove(turn, param, -value)

  override def getPlanningEntities: util.Collection[_] = Collections.singleton(turn)

  override def getPlanningValues: util.Collection[_] = param match {
    case "startHour" => Vector(turn.startHour)
    case "endHour" => Vector(turn.endHour)
  }


  override def isMoveDoable(scoreDirector: ScoreDirector): Boolean = {
    value != 0 && (turn.startHour + value) >= 0
  }

}


class ShiftMoveListFactory extends MoveListFactory[TimeTabler] {

  val range = -8 to 8

  override def createMoveList(solution: TimeTabler): util.List[_ <: Move] = {
    val turns = solution.getTurns
    turns.toIterator.filter(_.busy)
      .flatMap(turn => range.map(shift => new ShiftChangeMove(turn, shift)))
      .toVector
  }
}

class ShiftChangeMove(turn: Turn, value: Int) extends AbstractMove {

  override def doMoveOnGenuineVariables(scoreDirector: ScoreDirector): Unit = {
    scoreDirector.beforeVariableChanged(turn, "startHour")
    turn.startHour += value
    scoreDirector.afterVariableChanged(turn, "startHour")
    scoreDirector.beforeVariableChanged(turn, "endHour")
    turn.endHour += value
    scoreDirector.afterVariableChanged(turn, "endHour")
    scoreDirector.beforeVariableChanged(turn, "hourBreak")
    turn.hourBreak += value
    scoreDirector.afterVariableChanged(turn, "hourBreak")
  }

  override def createUndoMove(scoreDirector: ScoreDirector): Move
  = new ShiftChangeMove(turn, -value)

  override def getPlanningEntities: util.Collection[_] = Collections.singleton(turn)

  override def getPlanningValues: util.Collection[_] =
    Vector(turn.startHour, turn.endHour, turn.hourBreak)

  override def isMoveDoable(scoreDirector: ScoreDirector): Boolean = {
    value != 0 && (turn.startHour + value) >= 0
  }

}

class TurnsCountChangeMoveListFactory extends MoveListFactory[TimeTabler] {
  override def createMoveList(solution: TimeTabler): util.List[_ <: Move] = {
    val turns = solution.getTurns

    if (turns.size() > 100) {
      println("turns.size() > 100")
      return Collections.emptyList[Move]()
    }

    val maxEndOur = solution.endHours.max

    val turnsToAdd = solution.startHours.iterator.map(
      startHour => Turn(startHour, startHour + 9, hourBreak = startHour + 4))
      .takeWhile(_.endHour <= maxEndOur)

    turnsToAdd.map(turn => new AddTurnMove(turn, turns)).toVector ++
      turns.map(turn => new RemoveTurnMove(turn, turns)).toVector
  }
}

abstract class TurnsCountChangeMove(turn: Turn, turns: util.List[Turn]) extends AbstractMove {


  override def getPlanningEntities: util.Collection[_] = Collections.singleton(turn)

  override def getPlanningValues: util.Collection[_] = Vector(turn.startHour, turn.endHour, turn.hourBreak)

  override def isMoveDoable(scoreDirector: ScoreDirector): Boolean = true

}

class AddTurnMove(turn: Turn, turns: util.List[Turn]) extends TurnsCountChangeMove(turn, turns) {
  override def doMoveOnGenuineVariables(scoreDirector: ScoreDirector): Unit = {
    scoreDirector.beforeEntityAdded(turn)
    turns.add(turn)
    scoreDirector.afterEntityAdded(turn)
  }


  override def isMoveDoable(scoreDirector: ScoreDirector): Boolean = !turns.contains(turn)

  override def createUndoMove(scoreDirector: ScoreDirector): Move = new RemoveTurnMove(turn, turns)
}

class RemoveTurnMove(turn: Turn, turns: util.List[Turn]) extends TurnsCountChangeMove(turn, turns) {

  override def doMoveOnGenuineVariables(scoreDirector: ScoreDirector): Unit = {
    scoreDirector.beforeEntityRemoved(turn)
    turns.remove(turn)
    scoreDirector.afterEntityRemoved(turn)
  }

  override def createUndoMove(scoreDirector: ScoreDirector): Move = new AddTurnMove(turn, turns)

  override def isMoveDoable(scoreDirector: ScoreDirector): Boolean = turns.contains(turn)
}