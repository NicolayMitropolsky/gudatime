package ru.gudatime

import scala.collection.immutable.ListMap

/**
  * Created by nickl on 16.12.16.
  */
object StandardTurns {

  val startHour = 7

  val standards = Seq(
    Seq(7 -> 16, 8 -> 17, 9 -> 18, 10 -> 19), // 5/2
    Seq(7 -> 19, 8 -> 20, 9 -> 21, 10 -> 22), // 2/2
    Seq(16 -> 20, 17 -> 21, 18 -> 22) // "2 смена"
  ).map(_.map(p => (p._1 - startHour, p._2 - startHour)))

  def check(expectedRatios: Seq[Double]): (Seq[Turn]) => Double = {
    require(expectedRatios.size == standards.size, s"expectedRatios.size (${expectedRatios.size}) should be equal to standards.size (${standards.size})")
    require(expectedRatios.sum <= 1.0, s"expectedRatios.sum (${expectedRatios.sum}) should be <= 1.0")


    (turns: Seq[Turn]) => {

      val stdCounts = standards.map(group => turns.count(inGroup(group))) //.map(_.toDouble / turns.size)

      val diff = for ((expected, actual) <- expectedRatios zip stdCounts)
        yield {
          val roundedExpected = (expected * turns.size).round
          Math.max(0.0, roundedExpected - actual)
        }


      val nonStandartdExpected = ((1 - expectedRatios.sum) * turns.size).round
      val nonStandardActual = turns.size - stdCounts.sum
      diff.sum + Math.max(0.0, nonStandardActual - nonStandartdExpected)
    }

  }

  private def inGroup(group: Seq[(Int, Int)]) = (turn: Turn) => {
    group.exists(p => turn.startHour == p._1 && turn.endHour == p._2)
  }
}
