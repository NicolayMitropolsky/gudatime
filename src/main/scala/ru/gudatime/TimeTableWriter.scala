package ru.gudatime

import org.apache.poi.xssf.usermodel.{XSSFSheet, XSSFColor, XSSFWorkbook}
import scala.collection.JavaConversions.asScalaBuffer

import POIUtils._
import org.apache.poi.ss.usermodel.{HorizontalAlignment, CellStyle, Row}
import org.apache.poi.ss.util.CellRangeAddress
import java.awt.Color
import scala.collection.Map
import scala.concurrent.{Await, Future, future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.collection.immutable.IndexedSeq
import scala.concurrent.duration.Duration
import com.thoughtworks.xstream.XStream
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver

/**
 * Created with IntelliJ IDEA.
 * User: nickl
 * Date: 17.08.13
 * Time: 8:24
 * To change this template use File | Settings | File Templates.
 */

class TimeTableWriter(val workDayStart: Int, predictedDailyLoad: Iterable[IndexedSeq[Int]], result: Iterable[DayEntry]) {



  private def hourInterval(h: Int): String = hourInterval(h, h + 1)

  private def hourInterval(start: Int, end: Int): String = (workDayStart + start) + ":00 - " + (workDayStart + end) + ":00"

  val workbook = new XSSFWorkbook()

  val style1 = {
    val style1 = workbook.createCellStyle();
    style1.setFillForegroundColor(new XSSFColor(Color.GRAY));
    style1.setFillPattern(CellStyle.SOLID_FOREGROUND);
    style1.setAlignment(HorizontalAlignment.CENTER)
    val bor = 1.toShort
    style1.setBorderBottom(bor)
    style1.setBorderLeft(bor)
    style1.setBorderRight(bor)
    style1.setBorderTop(bor)
    style1
  }
  val style2 = {
    val style1 = workbook.createCellStyle();
    style1.setFillForegroundColor(new XSSFColor(Color.decode("#FDF7DD")));
    style1.setFillPattern(CellStyle.SOLID_FOREGROUND);
    style1.setAlignment(HorizontalAlignment.CENTER)
    val bor = 1.toShort
    style1.setBorderBottom(bor)
    style1.setBorderLeft(bor)
    style1.setBorderRight(bor)
    style1.setBorderTop(bor)
    style1
  }


  val cellOffset = 3


  def genTimeTable() = {

    val timetableSheet = workbook.createSheet()

    writeToRow((0 until result.head.calculatedUnits.length).map(i => hourInterval(i)), timetableSheet.createRow(0), cellOffset)

    for (DayEntry(day, calcUnits, table) <- result) {
      writeDayTimeTableToSheet(timetableSheet, day, calcUnits, table)
      timetableSheet.createRow(timetableSheet.getLastRowNum + 1)
    }

    (0 to 3).foreach(timetableSheet.autoSizeColumn)

  }


  private[this] def writeDayTimeTableToSheet(sheet: XSSFSheet, day: String, calcUnits: Seq[Int], table: TimeTabler) {

    val lastRowNum = sheet.getLastRowNum
    val headRow = sheet.createRow(lastRowNum + 1)
    headRow.createCell(0).setTypedValue(day)
    headRow.createCell(2).setTypedValue("Расчётная:")
    writeToRow(calcUnits, headRow, cellOffset)

    val turnsOfDay = table.turnsOfTheDay.sortBy(turn => turn.startHour + turn.endHour)

    val realLoadRow = sheet.createRow(lastRowNum + 2)

    writeToRow(calcUnits.indices.map(i => turnsOfDay.count(_.worksAt(i))), realLoadRow, cellOffset)
    realLoadRow.createCell(2).setTypedValue("Фактическая:")
    for ((turn, turnIndex) <- turnsOfDay.zipWithIndex) {

      val turnRow = sheet.createRow(lastRowNum + cellOffset + turnIndex)
      turnRow.createCell(1).setTypedValue(turnIndex + 1)
      turnRow.createCell(2).setTypedValue(
        turn.turnLength + " " +
          (workDayStart + turn.startHour) + "-" +
          (workDayStart + turn.endHour) +
          (if (turn.hasBreak) " пер.=" + (workDayStart + turn.hourBreak) else ""))
      val num = turnRow.getRowNum

      if (!turn.hasBreak) {
        //sheet.addMergedRegion(new CellRangeAddress(num, num, cellOffset + turn.startHour, cellOffset + turn.endHour - 1))
        for (i <- turn.startHour until turn.endHour ) {
        turnRow.getCell(cellOffset + i, Row.CREATE_NULL_AS_BLANK).setCellStyle(style1)
        }
      }
      else {

        for (i <- turn.startHour until turn.hourBreak ) {
          turnRow.getCell(cellOffset + i, Row.CREATE_NULL_AS_BLANK).setCellStyle(style1)
        }
        turnRow.getCell(cellOffset + turn.hourBreak, Row.CREATE_NULL_AS_BLANK).setCellStyle(style2)
        for (i <- (turn.hourBreak + 1) until turn.endHour ) {
          turnRow.getCell(cellOffset + i, Row.CREATE_NULL_AS_BLANK).setCellStyle(style1)
        }

      }
    }
  }

  private def writeToRow(load: Seq[_], loadrow: Row, cellOffset: Int) {
    for ((v, i) <- load.zipWithIndex) {
      loadrow.createCell(cellOffset + i).setTypedValue(v)
    }
  }

  def genStatistics(ec: ErlangC) = {

    val statSheet = workbook.createSheet()

    val headers = Seq(
      "Время",
      "Прогноз звонков",
      "Время обработки звонка, с",
      "Агентов на линии",
      "Прогнозируемые потери звонков",
      "Прогнозируемый процент потерянных звонков",
      "Прогнозируемый SL",
      "Прогнозируемая загруженность"
    )

    writeToRow(headers , statSheet.createRow(0), 0)


    //writeToRow((0 until result.head.load.length).map(i => hourInterval(i)), statSheet.createRow(0), cellOffset)

    for ((DayEntry(day, calcUnits, table), dayload) <- result zip predictedDailyLoad) {
      val dayrow = statSheet.createRow(statSheet.getLastRowNum + 1)
      val dayNameCell = dayrow.createCell(0)
      val style = dayNameCell.getCellStyle
      style.setAlignment(HorizontalAlignment.CENTER)
      dayNameCell.setCellStyle(style)
      dayNameCell.setTypedValue(day)
      statSheet.addMergedRegion(new CellRangeAddress(dayrow.getRowNum,dayrow.getRowNum,0, headers.size ))
      val turnsOfDay = table.turnsOfTheDay
      for(i <-  calcUnits.indices) {
        val row = statSheet.createRow(statSheet.getLastRowNum + 1)
        row.createCell(0).setTypedValue(hourInterval(i))
        row.createCell(1).setTypedValue(dayload(i))
        row.createCell(2).setTypedValue(ec.callTime)
        val operatorsCount = turnsOfDay.count(_.worksAt(i))
        row.createCell(3).setTypedValue(operatorsCount)

        val cSeq = ec.genErlangCSeq(dayload(i))
        //println(day +" "+ hourInterval(i)+" "+cSeq)
//        cSeq.find(_.avrWait < 60).foreach({ erlangEntry =>
//
//          //row.createCell(5).setTypedValue(erlangEntry.noAnswerProbability)
//          val lostCalls = 100 - erlangEntry.srvLvl
//
//          row.createCell(4).setTypedValue(dayload(i) * lostCalls/100)
//          row.createCell(5).setTypedValue(lostCalls)
//
//
//        })

        cSeq.find(_.operatorsNumber >= operatorsCount).foreach({ erlangEntry =>

          row.createCell(4).setTypedValue(Math.round(dayload(i) * erlangEntry.noAnswerProbability / 100))
          row.createCell(5).setTypedValue(Math.round(erlangEntry.noAnswerProbability))
          row.createCell(6).setTypedValue(erlangEntry.srvLvl)
          row.createCell(7).setTypedValue(erlangEntry.operatorOccupation)

        })

      }

    }

    //(0 to headers.size).foreach(statSheet.autoSizeColumn)

  }

}

case class DayEntry(day:String, calculatedUnits:IndexedSeq[Int], table:TimeTabler)

