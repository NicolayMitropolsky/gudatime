package ru.gudatime.utils

import org.optaplanner.core.api.domain.valuerange.CountableValueRange
import org.optaplanner.core.api.score.Score

/**
  * Created by nickl on 04.01.17.
  */
object OptaPlannerUtils {

  implicit class CountableValueRangesOpts[T](val range: CountableValueRange[T]) extends IndexedSeq[T] {

    override def length: Int = range.getSize.toInt

    override def apply(idx: Int): T = range.get(idx)
  }

  implicit def scoreOrderingOps[T <: Score[T]](score: T): Ordering[T]#Ops = Ordering.ordered[T].mkOrderingOps(score)

}
