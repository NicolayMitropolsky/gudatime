package ru.gudatime.utils

import java.awt.event.{ActionEvent, ActionListener}
import javax.swing.event.{ChangeEvent, ChangeListener}
import javax.swing.JTextField
import javax.swing.text.{JTextComponent, PlainDocument}

/**
 * Created with IntelliJ IDEA.
 * User: nickl
 * Date: 16.07.13
 * Time: 17:21
 * To change this template use File | Settings | File Templates.
 */
object SwingUtils {

  implicit def funcToActionListener(f: ActionEvent => Any) = new ActionListener {
    def actionPerformed(e: ActionEvent) {
      f(e)
    }
  }

  implicit def funcToActionListener(f: () => Any) = new ActionListener {
    def actionPerformed(e: ActionEvent) {
      f()
    }
  }

  implicit def funcToActionListener(f: (ChangeEvent) => Any) = new ChangeListener {
    def stateChanged(e: ChangeEvent) {f(e)}
  }

  implicit def funcToRunnable(f:  => Any) = new Runnable {
    def run() {f}
  }

  def invokeInSwing(f: => Any){
    javax.swing.SwingUtilities.invokeLater(new Runnable {
      def run {
        f
      }
    })
  }

}

trait IntFilter {
  this:JTextComponent =>

  this.getDocument.asInstanceOf[PlainDocument].setDocumentFilter(new MyIntFilter)

  def intValue = this.getText.toInt

}


