package ru.gudatime

import org.optaplanner.core.api.score.buildin.hardsoftdouble.HardSoftDoubleScore
import org.scalatest.FunSpec

import scala.collection.JavaConverters._
import ru.gudatime.utils.OptaPlannerUtils.scoreOrderingOps

/**
  * Created by nickl on 26.11.16.
  */
class EvalTest extends FunSpec {

  describe("Scoring") {

    it("sunday ") {
      val plan = Seq(5, 5, 4, 5, 4, 3, 4, 4, 3, 3, 2, 1, 1)
      val score1 = scoreTurns(plan, Seq(
            Turn(8, 4, hourBreak = 6), Turn(0, 10, hourBreak = 6), Turn(4, 8, hourBreak = 10),
        Turn(0, 4, hourBreak = 8), Turn(4, 13, hourBreak = 8), Turn(0, 4, hourBreak = 11),
        Turn(0, 4, hourBreak = 11), Turn(4, 4, hourBreak = 7), Turn(0, 11, hourBreak = 7),
        Turn(6, 10, hourBreak = 10)))


      val score2 = scoreTurns(plan, Seq(
        Turn(8, 4, hourBreak = 6), Turn(0, 10, hourBreak = 6), Turn(8, 8, hourBreak = 10),
        Turn(0, 9, hourBreak = 5), Turn(4, 13, hourBreak = 8), Turn(0, 9, hourBreak = 5),
        Turn(0, 4, hourBreak = 11), Turn(4, 4, hourBreak = 7), Turn(0, 11, hourBreak = 7),
        Turn(10, 10, hourBreak = 10)))

      println("score1 = " + score1)
      println("score2 = " + score2)

    }

    it("satuday ") {
      val plan = Seq(6,	6,	6,	5,	5,	4,	4,	4,	4,	2,	2,	2,	1      )
      val score1 = scoreTurns(plan, Seq(
        Turn(0, 4, hourBreak= -1), Turn(0, 8, hourBreak= -1), Turn(0, 7, hourBreak= -1),
        Turn(8, 8, hourBreak=7), Turn(5, 5, hourBreak=10), Turn(0, 10, hourBreak=6),
        Turn(4, 4, hourBreak=8), Turn(4, 13, hourBreak=9), Turn(8, 8, hourBreak=6),
        Turn(0, 12, hourBreak=7), Turn(0, 10, hourBreak=6), Turn(6, 6, hourBreak=11)))


      val score2 = scoreTurns(plan, Seq(
          Turn(8, 13, hourBreak= -1), Turn(0, 8, hourBreak= -1), Turn(0, 7, hourBreak= -1),
          Turn(8, 8, hourBreak=7), Turn(5, 5, hourBreak=10), Turn(0, 10, hourBreak=6),
          Turn(4, 4, hourBreak=8), Turn(0, 7, hourBreak=9), Turn(8, 8, hourBreak=6),
          Turn(0, 12, hourBreak=7), Turn(0, 10, hourBreak=6), Turn(6, 6, hourBreak=11)))

      println("score1 = " + score1)
      println("score2 = " + score2)

    }


    it("friday") {
      val plan = Seq(4, 8, 10, 11, 10, 9, 9, 12, 11, 11, 8, 5, 4, 3, 3)
      val check = StandardTurns.check(Seq(0.2, 0.0, 0.2))
      val score1 = scoreTurns(plan, Seq(
        Turn(1, 13, hourBreak = 5),
        Turn(3, 15, hourBreak = 7),
        Turn(0, 12, hourBreak = 7),
        Turn(0, 9, hourBreak = 4),
        Turn(0, 12, hourBreak = 8),
        Turn(3, 15, hourBreak = 11),
        Turn(1, 10, hourBreak = 5),
        Turn(1, 11, hourBreak = 6),
        Turn(0, 9, hourBreak = 4),
        Turn(4, 15, hourBreak = 9),
        Turn(2, 11, hourBreak = 6),
        Turn(1, 10, hourBreak = 6)),
        check
      )

      val score2 = scoreTurns(plan, Seq(
        Turn(1, 13, hourBreak = 5),
        Turn(3, 15, hourBreak = 7),
        Turn(0, 12, hourBreak = 7),
        Turn(0, 9, hourBreak = 4),
        Turn(0, 9, hourBreak = 4),
        Turn(0, 9, hourBreak = 4),
        Turn(0, 9, hourBreak = 4),
        Turn(0, 9, hourBreak = 4),
        Turn(0, 9, hourBreak = 4),
        Turn(0, 9, hourBreak = 4),
        Turn(0, 9, hourBreak = 4),
        Turn(0, 9, hourBreak = 4),
        Turn(0, 12, hourBreak = 8),
        Turn(3, 15, hourBreak = 11),
        Turn(1, 10, hourBreak = 5),
        Turn(1, 11, hourBreak = 6),
        Turn(0, 9, hourBreak = 4),
        Turn(4, 15, hourBreak = 9),
        Turn(9, 13, hourBreak = 6),
        Turn(9, 13, hourBreak = 6),
        Turn(9, 13, hourBreak = 6),
        Turn(9, 13, hourBreak = 6),
        Turn(9, 13, hourBreak = 6),
        Turn(9, 13, hourBreak = 6),
        Turn(9, 13, hourBreak = 6),
        Turn(9, 13, hourBreak = 6),
        Turn(9, 13, hourBreak = 6),
        Turn(9, 13, hourBreak = 6),
        Turn(9, 13, hourBreak = 6),
        Turn(1, 10, hourBreak = 6)),
        check
      )


      println("score1 = " + score1)
      println("score2 = " + score2)

      assert(score2.getHardScore == 0)
      assert(score1 < score2)

    }

  }

  private def scoreTurns(plan: Seq[Int], turns: Seq[Turn], standardTurnsRatioDeviation: (Seq[Turn]) => Double = (any) => 0.0) = {
    val calc = new TimeTableScoreCalc()
    for (turn <- turns ) {
      if(calc.invalidBreak(turn))
        throw new IllegalArgumentException("Invalid break :" + turn)
    }

    val tabler = new TimeTabler(
      plan
    )
    tabler.standardTurnsRatioDeviation = standardTurnsRatioDeviation
    tabler.turns = turns.asJava
    val score = calc.calculateScore(tabler)
    score
  }
}
