package ru.gudatime

import org.optaplanner.core.api.score.Score
import org.scalatest.FunSpec
import org.scalatest.Matchers._

/**
  * Created by nickl on 04.01.17.
  */
class StandardTurnsTest  extends FunSpec {

  describe("Standard Turns checking") {
    it("shold be zero for 3/3 ") {
     StandardTurns.check(Seq(0.34, 0.33, 0.33))(Seq(
        Turn(0, 9, hourBreak = 4),
        Turn(1, 13, hourBreak = 6),
        Turn(9, 13, hourBreak = 1)
     )) shouldBe 0.0
    }

    it("shold be non zero for 3/3 + non-standard 4-th") {
     StandardTurns.check(Seq(0.34, 0.33, 0.33))(Seq(
        Turn(0, 9, hourBreak = 4),
        Turn(1, 13, hourBreak = 6),
        Turn(9, 13, hourBreak = 1),
        Turn(0, 13, hourBreak = 1)
     )) should be > 0.1
    }
    it("shold be zero for half of first type when others are not restricted") {
      StandardTurns.check(Seq(0.5, 0.0, 0.0))(Seq(
        Turn(0, 9, hourBreak = 4),
        Turn(0, 9, hourBreak = 4),
        Turn(0, 9, hourBreak = 4),
        Turn(1, 13, hourBreak = 6),
        Turn(9, 13, hourBreak = 1),
        Turn(0, 13, hourBreak = 1)
      )) shouldBe 0.0
    }

    it("shold be zero for half of first type when more than one of first type") {
      StandardTurns.check(Seq(0.5, 0.0, 0.0))(Seq(
        Turn(0, 9, hourBreak = 4),
        Turn(0, 9, hourBreak = 4),
        Turn(0, 9, hourBreak = 4),
        Turn(0, 9, hourBreak = 4),
        Turn(0, 9, hourBreak = 4),
        Turn(1, 13, hourBreak = 6),
        Turn(9, 13, hourBreak = 1),
        Turn(0, 13, hourBreak = 1)
      )) shouldBe 0.0
    }

    it("shold be zero for thusday at 60") {
      StandardTurns.check(Seq(0.60, 0.0, 0.0))(Seq(
        Turn(0, 12, hourBreak= 6),
        Turn(2, 11, hourBreak= 7),
        Turn(1, 10, hourBreak= 6),
        Turn(5, 15, hourBreak= 10),
        Turn(1, 10, hourBreak= 6),
        Turn(5, 15, hourBreak= 10),
        Turn(8, 8, hourBreak= -1),
        Turn(10, 10, hourBreak= 7),
        Turn(10, 10, hourBreak= 10),
        Turn(1, 10, hourBreak= 6),
        Turn(9, 9, hourBreak= 10),
        Turn(8, 8, hourBreak= 9),
        Turn(3, 12, hourBreak= 8),
        Turn(1, 10, hourBreak= 6),
        Turn(4, 4, hourBreak= 11),
        Turn(3, 12, hourBreak= 8),
        Turn(1, 13, hourBreak= 9),
        Turn(0, 11, hourBreak= 7),
        Turn(5, 15, hourBreak= 9),
        Turn(3, 12, hourBreak= 7),
        Turn(7, 7, hourBreak= 7),
        Turn(4, 4, hourBreak= -1)
      ).filter(_.busy)) shouldBe 0.0
    }

    it("shold be zero for tuesday at 60") {
      StandardTurns.check(Seq(0.30, 0.20, 0.0))(Seq(
        Turn(7, 7, hourBreak= 11),
        Turn(8, 4, hourBreak= 11),
        Turn(9, 4, hourBreak= 9),
        Turn(1, 12, hourBreak= 8),
        Turn(1, 13, hourBreak= 8),
        Turn(2, 14, hourBreak= 10),
        Turn(1, 10, hourBreak= 5),
        Turn(1, 10, hourBreak= 6),
        Turn(10, 7, hourBreak= 4),
        Turn(3, 15, hourBreak= 8),
        Turn(1, 11, hourBreak= 6),
        Turn(7, 7, hourBreak= 11),
        Turn(6, 4, hourBreak= 11),
        Turn(1, 11, hourBreak= 5),
        Turn(4, 15, hourBreak= 11),
        Turn(10, 5, hourBreak= 9),
        Turn(0, 9, hourBreak= 4),
        Turn(4, 4, hourBreak= 4),
        Turn(9, 7, hourBreak= 4),
        Turn(7, 4, hourBreak= 4),
        Turn(6, 5, hourBreak= 4),
        Turn(0, 9, hourBreak= 5),
        Turn(0, 12, hourBreak= 4),
        Turn(9, 6, hourBreak= 11),
        Turn(0, 9, hourBreak= 4),
        Turn(10, 15, hourBreak= 5)
      ).filter(_.turnLength > 0)) shouldBe 0.0
    }

    it("shold be zero for friday 0.2 0.0 0.2") {
      StandardTurns.check(Seq(0.20, 0.00, 0.20))(Seq(
        Turn(1, 10, hourBreak = 5),
        Turn(1, 10, hourBreak = 5),
        Turn(1, 10, hourBreak = 5),
        Turn(1, 10, hourBreak = 5),
        Turn(1, 10, hourBreak = 5),
        Turn(1, 10, hourBreak = 5),
        Turn(1, 10, hourBreak = 5),
        Turn(1, 10, hourBreak = 5),
        Turn(9, 13, hourBreak = 8),
        Turn(9, 13, hourBreak = 8),
        Turn(9, 13, hourBreak = 8),
        Turn(9, 13, hourBreak = 8),
        Turn(9, 13, hourBreak = 8),
        Turn(9, 13, hourBreak = 8),
        Turn(9, 13, hourBreak = 8),
        Turn(9, 13, hourBreak = 8),
        Turn(9, 13, hourBreak = 8)
).filter(_.turnLength > 0)) shouldBe 0.0
    }
  }


}
